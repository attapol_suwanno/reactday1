import React from 'react';

import './App.css';

import Calculator from './componentCalculator/Calculator';


const App = () => {
  return (
    <div>
      <Calculator />
    </div>

  )
}

export default App;
