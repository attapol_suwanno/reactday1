import React from 'react';
import '../App.css';
import Display from './Display';
import Numpad from './Numpad';
import { Alert } from 'reactstrap';


class Calculator extends React.Component {
    constructor(prors) {
        super(prors)
        this.state = {

            numberinput: " ",
            result:0


        }
    }
    callTaxt = (num) => {
        let lastnumInelement = this.state.numberinput 
        let lastnum = lastnumInelement.substr(-1);
        if (isNaN(num) && (lastnum == "+" || lastnum == "-" || lastnum == "*" || lastnum == "/")) {
            alert("Error")
        } else {

            this.setState({
                numberinput: this.state.numberinput + num
              
            })
        }
    }

    callEqual = () => {
        this.result= eval(this.state.numberinput)
        this.setState({

      numberinput:(this.state.numberinput) +"="+ this.result
        })
    }
    callClearn = () => {
        this.setState({
            numberinput: " "
        })
    }
    render() {
        return (
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-12">
                        <table>
                            <Display min={this.state.numberinput} />
                            <Numpad OnClick={this.callTaxt} OnEqual={this.callEqual} onClearn={this.callClearn} />

                        </table>
                    </div>
                </div>
            </div>

        )
    }

}

export default Calculator;