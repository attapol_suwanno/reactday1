import React from 'react';
import '../App.css';


const Numpad = (props) => {
    return (

        <table>
            <tr>
                <td><input type="button" value="1" onClick={() => props.OnClick('1')} /> </td>
                <td><input type="button" value="2" onClick={() => props.OnClick('2')} /> </td>
                <td><input type="button" value="3" onClick={() => props.OnClick('3')} /> </td>
                <td><input type="button" value="+" onClick={() => props.OnClick('+')} /> </td>
            </tr>
            <tr>
                <td><input type="button" value="4" onClick={() => props.OnClick('4')} /> </td>
                <td><input type="button" value="5" onClick={() => props.OnClick('5')} /> </td>
                <td><input type="button" value="6" onClick={() => props.OnClick('6')} /> </td>
                <td><input type="button" value="-" onClick={() => props.OnClick('-')} /> </td>
            </tr>
            <tr>
                <td><input type="button" value="7" onClick={() => props.OnClick('7')} /> </td>
                <td><input type="button" value="8" onClick={() => props.OnClick('8')} /> </td>
                <td><input type="button" value="9" onClick={() => props.OnClick('9')} /> </td>
                <td><input type="button" value="*" onClick={() => props.OnClick('*')} /> </td>
            </tr>
            <tr>
                <td><input type="button" value="0" onClick={() => props.OnClick('0')} /> </td>

                <td colspan="2"> <input id="buttom1" type="button" value="=" onClick={() => props.OnEqual()} /> </td>
                <td><input type="button" value="÷" onClick={() => props.OnClick('/')} /> </td>
            </tr>
            <tr>
                <td colspan="4"><input id="clearbuttom" type="button" value="clear" onClick={() => props.onClearn()} /> </td>
            </tr>

        </table>



    )


}

export default Numpad;